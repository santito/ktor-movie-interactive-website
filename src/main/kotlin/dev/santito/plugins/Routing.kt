package dev.santito.plugins

import dev.santito.routes.movieRouting
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.routing.*

fun Application.configureRouting() {
    routing {
        static("/static") {
            resources("files")
            resources("templates")
        }
        movieRouting()
        }
}
