package dev.santito.routes

import dev.santito.models.Movie
import dev.santito.models.movies
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.freemarker.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import java.io.File

fun Route.movieRouting() {
    get("/") {
        call.respondRedirect("movies")
    }
    route("movies") {
        get {
            call.respond(FreeMarkerContent("index.ftl", mapOf("movies" to movies)))
        }
        get("new") {
            call.respond(FreeMarkerContent("new.ftl", model = null))
        }
        post {
            var title = ""
            var year = ""
            var genre = ""
            var director = ""
            var image = ""

            val multipartData = call.receiveMultipart()

            multipartData.forEachPart { part ->
                when (part) {
                    is PartData.FormItem -> {
                        when(part.name){
                            "title" -> title = part.value
                            "year" -> year = part.value
                            "genre" -> genre = part.value
                            "director" -> director = part.value
                        }
                    }

                    is PartData.FileItem -> {
                        image = part.originalFileName as String
                        val fileBytes = part.streamProvider().readBytes()

                        File("uploads/$image").writeBytes(fileBytes)
                    }

                    else -> {}
                }
                part.dispose()
            }


            val newEntry = Movie(
                title = title,
                year = year,
                genre = genre,
                director = director,
                image = image
            )
            movies.add(newEntry)
            call.respondRedirect("movies/detail/${newEntry.id}")
        }
        get("detail/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            call.respond(FreeMarkerContent("show.ftl", mapOf("movie" to movies.find { it.id == id })))
        }
    }
    get("/uploads/{imageName}") {
        val imageName = call.parameters["imageName"]
        val file = File("./uploads/$imageName")
        if(file.exists()){
            call.respondFile(File("./uploads/$imageName"))
        }
        else{
            call.respondText("Image not found", status = HttpStatusCode.NotFound)
        }
    }
}
