package dev.santito.models

import java.util.concurrent.atomic.AtomicInteger

class Movie(
    val id: Int = idCounter.getAndIncrement(),
    var title: String,
    var year: String,
    var genre: String,
    var director: String,
    var image: String
){
    companion object {
        private val idCounter = AtomicInteger()
    }
}

val movies = mutableListOf<Movie>()
