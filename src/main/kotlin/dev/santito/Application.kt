package dev.santito

import io.ktor.server.application.*
import dev.santito.plugins.configureRouting
import dev.santito.plugins.configureTemplating

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    configureRouting()
    configureTemplating()
}
