<#macro header>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Santito Kotlin</title>
        <link rel="stylesheet" href="/static/styles.css">
    </head>
    <body>
    <img src="/static/ktor_logo.png" alt="ktor logo">
    <h1>Kotlin Ktor Movie Page</h1>
    <p><i>Powered by Santi!</i></p>
    <hr>
    <#nested>
    <nav>
        <a href="/movies/new">Create movie</a>
        <a href="/">Back to the main page</a>
        <a href="https://santito.is-a.dev/">About us</a>
    </nav>
    </body>
    </html>
</#macro>