<#import "_layout.ftl" as layout />
<@layout.header>
    <div>
        <h3>Create movie</h3>
        <form action="/movies" method="post" enctype="multipart/form-data">
            <p>
                <label>
                    Title:
                    <input type="text" name="title" required>
                </label>
            </p>
            <p>
                <label>
                    Year:
                    <input type="text" name="year" required>
                </label>
            </p>
            <p>
                <label>
                    Genre:
                    <input type="text" name="genre" required>
                </label>
            </p>
            <p>
                <label>
                    Director:
                    <input type="text" name="director" required>
                </label>
            </p>
            <p>
                <label>
                    image:
                    <input type="file" name="image" accept="image/png, image/jpeg" required>
                </label>
            </p>
            <p>
                <input type="submit">
            </p>
        </form>
    </div>
</@layout.header>