<#-- @ftlvariable name="movie" type="dev.santito.models.Movie" -->
<#import "_layout.ftl" as layout />
<@layout.header>
    <div>
        <h3>
            ${movie.title}
        </h3>
        <table>
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Genre</th>
                    <th>Director</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>${movie.year}</td>
                    <td>${movie.genre}</td>
                    <td>${movie.director}</td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div>
            <img src="/uploads/${movie.image}" alt="${movie.id} img">
        </div>
        <hr>
    </div>
</@layout.header>