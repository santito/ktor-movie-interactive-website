<#-- @ftlvariable name="movies" type="kotlin.collections.List<dev.santito.models.Movie>" -->
<#import "_layout.ftl" as layout />
<@layout.header>
    <#list movies?reverse as movie>
        <article>
            <img src="/uploads/${movie.image}" alt="${movie.id} img">
            <div>
                <h3>
                    <a href="/movies/detail/${movie.id}">Title: ${movie.title}</a>
                </h3>
                <p>
                    Year: ${movie.year}
                    Genre: ${movie.genre}
                    Director: ${movie.director}
                </p>
            </div>
        </article>
    </#list>
    <hr>
</@layout.header>